<?php
setlocale(LC_ALL,'pt_BR.UTF8');
mb_internal_encoding('UTF8');
mb_regex_encoding('UTF8');

$stCurLogFileName = file_get_contents('infusion_token.txt');
$stCurLogFileName = json_decode($stCurLogFileName, true);
$stCurLogFileName = json_decode($stCurLogFileName, true);

// list DDD
$ddd_list = array(
    '11',
    '12',
    '13',
    '14',
    '15',
    '16',
    '17',
    '18',
    '19',
    '21',
    '22',
    '24',
    '27',
    '28',
    '31',
    '32',
    '33',
    '34',
    '35',
    '37',
    '38',
    '41',
    '42',
    '43',
    '44',
    '45',
    '46',
    '47',
    '48',
    '49',
    '51',
    '53',
    '54',
    '55',
    '61',
    '62',
    '63',
    '64',
    '65',
    '66',
    '67',
    '68',
    '69',
    '71',
    '73',
    '74',
    '75',
    '77',
    '79',
    '81',
    '82',
    '83',
    '84',
    '85',
    '86',
    '87',
    '88',
    '89',
    '91',
    '92',
    '93',
    '94',
    '95',
    '96',
    '97',
    '98',
    '99'
);

// list mail
$mail_list = array(
    'Hotmail',
    'hot.com',
    'hot.com.br',
    'rotmail.com.br',
    'rotmail.com',
    'hotmeil.com.br',
    'hotmeil.com',
    'htmail.com',
    'hotmaul.com',
    'hotmaul.com.br',
    'otmail.com',
    'otmail.com.br',
    'hoymail.com',
    'hoymail.com.br',
    'htmail.com.br',
    'hotmal.com',
    'hotmal.com.br',
    'hmail.com',
    'hmail.com.br',
    'hatmail.com',
    'hatmail.com.br',
    'hotmis.com',
    'hotmis.com.br',
    'hoymail.com',
    'hotamail.com',
    'hotamail.com.br',
    'hatmail.com',
    'hotail.com',
    'hmail.com.br',
    'hmail.com',
    'hotmail.co',
    'hotmaol.com',
    'hotmaol.com.br',
    'hotmsil.com.br',
    'hotmsil.com',
    'hptmail.com',
    'hmail.com',
    'jhotmail.com',
    'hot.mail.com',
    'hotimaio.com',
    'homail.com',
    'hotmeil.com',
    'gmail.comr',
    'gmil.com.br',
    'gmil.com',
    'gmil.com.br',
    'gmail.co',
    'gimail.com',
    'gimail.com.br',
    'gamil.com',
    'gmail.com.br',
    'gemail.com',
    'gmqil.com',
    'gmqil.com.br',
    'GmIl.com',
    'GmIl.com.br',
    'glm.com',
    'gamel.com',
    'giml.com',
    'Gmaiil.com',
    'Gmaiil.com.br',
    'giml.com.br',
    'gamel.com.br',
    'gnail.com',
    'yahoo.co',
    'yahoo.com.bt',
    'yhoo.com.br'
);

// creação da variavel de debug
$html = '';

// add as tags na lead
function add_tag($id,$tag,$access_token){
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://api.infusionsoft.com/crm/rest/v1/tags/".$tag."/contacts/?access_token=".$access_token,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => '{
                      "ids": [
                        '.$id.'
                      ]
                    }',
      CURLOPT_COOKIE => "__cfduid=d4bf1e21eb1a03bd2cee49e145ac975891587159073; JSESSIONID=39AA3C09B0AC94D42B2A4875B06EA6A1",
      CURLOPT_HTTPHEADER => array(
        "accept: application/json",
        "content-type: application/json"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
}

// sistema de verificação de numero e corrigir (e debugar numero).
function ddi_check ($value, $ddd_list, $access_token){
    $status = 0;
    $insert_nine = 0;
    $return = '';
    $value = str_replace(') ', '', $value);
    $value = str_replace(')', '', $value);
    $value = str_replace('#', '', $value);
    $value = str_replace('-', '', $value);
    $value = str_replace(' ', '', $value);
    $value = str_replace('_', '', $value);
    $value = str_replace('(', '', $value);
    $value = preg_replace("/[^0-9]/", "", $value);
    $ddi = '55';
    if(substr($value, 0, 1) == '0'){
        $value = substr_replace($value, '', 0, 1);
    }
    if(substr($value, 0, 4) == '5555'){
        $result = substr_replace($value, '', 0, 4);
        $ddd = substr($value, 0, 2);
        $return .= 'has DDI and DDD RS <br>';

        if(in_array($ddd, $ddd_list)){
            $return .= 'DDD is valid <br>';
        }else{
            $return .= 'DDD not is valid <br>';
        }

        if(strlen($result) == 9 || strlen($result) == 8){
            if(strlen($result) == 8){
                $result = '9'.$result;
                $insert_nine = 1;
            }
            $return .= '--number valid <br>';
        }else{
            $return .= '--number invalid <br>';
        }
    }else if(substr($value, 0, 2) == '55'){
        if(strlen($value) <= 11){
            $ddd = '55';
            $result = substr_replace($value, '', 0, 2);
            $return .= '--This number is realy RS<br>';
        }else{
            $ddd = substr($value, 0, 2);
            $result = substr_replace($value, '', 0, 2);
            $return .= '--This number not is realy RS<br>';
        }

        if(in_array($ddd, $ddd_list)){
            $return .= 'DDD is valid <br>';
        }else{
            $status = 0;
            $return .= 'DDD not is valid <br>';
        }

        if(strlen($result) == 9 || strlen($result) == 8){
            if(strlen($result) == 8){
                $result = '9'.$result;
                $insert_nine = 1;
            }
            $return .= '--number valid <br>';
        }else{
            $return .= '--number invalid <br>';
        }
    }else {

        if(strlen($value) <= 11){
            $ddd = substr($value, 0, 2);
            $result = substr_replace($value, '', 0, 2);
            $return .= '--This number is valid <br>';
        }else{
            $status = 1;
            $ddd = substr($value, 0, 2);
            $result = substr_replace($value, '', 0, 2);
            $return .= '--This number invalid<br>';
        }

        if(in_array($ddd, $ddd_list)){
            $return .= 'DDD is valid <br>';
        }else{
            $status = 1;
            $return .= 'DDD not is valid <br>';
        }

        if(strlen($result) == 9 || strlen($result) == 8){
            if(strlen($result) == 8){
                $result = '9'.$result;
                $return .= '--Add the number 9<br>';
            }
            $return .= 'number valid <br>';
        }else{
            $status = 1;
            $return .= '--number invalid <br>';

        }
    }

    $return .= $value.'<br>';
    $return .= $ddi.' ('.$ddd.') '.$result.' With acount '.strlen($result).'<br>';
    $return .= '------------------------------<br>';

    if($status == 1){
        add_tag($_POST['id'],'160',$stCurLogFileName['access_token']); // tag Telefone-Invalido
    }else{
        add_tag($_POST['id'],'164',$stCurLogFileName['access_token']); // tag Telefone-Valido
    }

    if($insert_nine == 0){
        add_tag($_POST['id'],'200',$stCurLogFileName['access_token']); // tag Telefone-NaoTratado
    }

    if($result == ''){
        add_tag($_POST['id'],'200',$stCurLogFileName['access_token']); // tag Telefone-NaoTratado
    }else{
        add_tag($_POST['id'],'200',$stCurLogFileName['access_token']); // tag Telefone-NaoTratado
        return $ddi.$ddd.$result;
    }
}

// corrigir nome e debugar.
$lname_n;
$acentos  =  'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
$sem_acentos  =  'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
function name($fname, $access_token){

    if(empty($fname)){
        add_tag($_POST['id'],'162',$stCurLogFileName['access_token']); // tag Nome-Invalido
        add_tag($_POST['id'],'196',$stCurLogFileName['access_token']); // tag Nome-NaoTratado
    }else{
        add_tag($_POST['id'],'166',$stCurLogFileName['access_token']); // tag Nome-Valido

        $acentos  =  'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $sem_acentos  =  'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $string = utf8_decode($fname);

        $string = strtr($string, utf8_decode($acentos), $sem_acentos);

        $string = ltrim($string);

        $name = explode(' ', $string);
        $lname_n = '';
        $i = 0;
        foreach ($name as &$key) {
            if($i > 0){
                $lname_n .= $key.' ';

            }else {
                $i++;
            }
        }
        $fname_r = array('name' => $name[0],'last_name' => $lname_n);
        return $fname_r;
    }
}

// debugar Email
function email($email, $mail_list,$access_token){
    $return = $email;
    $email = explode('@',$email);
    if(isset($email[1])){

        foreach ($mail_list as &$key){
            if($key == $email[1]){
                add_tag($_POST['id'],'158',$stCurLogFileName['access_token']); // tag Email-Invalido
            }else{
                add_tag($_POST['id'],'168',$stCurLogFileName['access_token']); // tag Email-Valido
            }
        }
    }else{}

    return $return;
}

$lname_r = name($_POST['fname'], $stCurLogFileName['access_token'])['last_name'].' '.$_POST['lname'];
$mail_r = email($_POST['mail'],$mail_list, $stCurLogFileName['access_token']);

$lname_r = utf8_decode($lname_r);;
$lname_r = strtr($lname_r, utf8_decode($acentos), $sem_acentos);
$lname_r = ltrim($lname_r);

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.infusionsoft.com/crm/rest/v1/contacts/".$_POST['id']."?access_token=".$stCurLogFileName['access_token'],
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "PATCH",
  CURLOPT_POSTFIELDS => '{
      "custom_fields": [
          {
            "id": 8,
            "content": "'.ucwords(strtolower(name($_POST['fname'], $stCurLogFileName['access_token'])['name'])).'"
          },
          {
            "id": 10,
            "content": "'.ddi_check($_POST['phone'], $ddd_list,$stCurLogFileName['access_token']).'"
          },
          {
            "id": 12,
            "content": "'.$mail_r.'"
          }
        ]
    }',
  CURLOPT_COOKIE => "__cfduid=d4bf1e21eb1a03bd2cee49e145ac975891587159073; JSESSIONID=39AA3C09B0AC94D42B2A4875B06EA6A1",
  CURLOPT_HTTPHEADER => array(
    "accept: application/json",
    "authorization: Basic ".base64_encode('GjcpJDKLm1KE6E7LC4GqDtixPAtsh88i:ghbmawCyhkCL7gGB'),
    "content-type: application/json"
  ),
));


$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);


$html = '{
    "custom_fields": [
        {
          "id": 8,
          "content": "'.ucwords(strtolower(name($_POST['fname'], $stCurLogFileName['access_token'])['name'])).'"
        },
        {
          "id": 10,
          "content": "'.ddi_check($_POST['phone'], $ddd_list,$stCurLogFileName['access_token']).'"
        },
        {
          "id": 12,
          "content": "'.$mail_r.'"
        }
      ],
  }';

function m_log($arMsg){
    //define empty string
    $stEntry="";
    //get the event occur date time,when it will happened
    $arLogData['event_datetime']='['.date('D Y-m-d h:i:s A').'] [client '.$_SERVER['REMOTE_ADDR'].']';
    //if message is array type
    if(is_array($arMsg))
    {
        //concatenate msg with datetime
        foreach($arMsg as $msg)
        $stEntry.=$arLogData['event_datetime']."<br> ".$msg."";
    } else {   //concatenate msg with datetime
        $stEntry.=$arLogData['event_datetime']."<br> ".$arMsg."";
    }

    //create file with current date name
    $stCurLogFileName='log_'.date('Ymd').'.html';
    //open the file append mode,dats the log file will create day wise
    $fHandler=fopen('infusion_data_retorn_'.$stCurLogFileName,'a+');
    //write the info into the file
    fwrite($fHandler,$stEntry);
    //close handler
    fclose($fHandler);
}
m_log('<pre>'.json_encode($_POST).' '.$html.'</pre>');

// if (json_decode($response, true)['detail']['errorcode'] == "keymanagement.service.access_token_expired") {
//     $curl = curl_init();
//
//     curl_setopt_array($curl, array(
//       CURLOPT_URL => "https://api.infusionsoft.com/token/",
//       CURLOPT_RETURNTRANSFER => true,
//       CURLOPT_ENCODING => "",
//       CURLOPT_MAXREDIRS => 10,
//       CURLOPT_TIMEOUT => 30,
//       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//       CURLOPT_CUSTOMREQUEST => "POST",
//       CURLOPT_POSTFIELDS => "grant_type=authorization_code&refresh_token=".$stCurLogFileName['refresh_token'],
//       CURLOPT_HTTPHEADER => array(
//         "Authorization: basic ".base64_encode('GjcpJDKLm1KE6E7LC4GqDtixPAtsh88i:ghbmawCyhkCL7gGB'),
//         "content-type: application/x-www-form-urlencoded",
//       ),
//     ));
//
//     $response = curl_exec($curl);
//     $err = curl_error($curl);
//
//     curl_close($curl);
//
//     if ($err) {
//       echo "cURL Error #:" . $err;
//     } else {
//       // echo $response;
//     }
//     $response = json_decode($response,true);
//     echo "cURL Error #:" . $err;

    // function m_log($arMsg){
    //     //define empty string
    //     $stEntry="";
    //     //get the event occur date time,when it will happened
    //     $arLogData['event_datetime']='['.date('D Y-m-d h:i:s A').'] [client '.$_SERVER['REMOTE_ADDR'].']';
    //     //if message is array type
    //     if(is_array($arMsg))
    //     {
    //     //concatenate msg with datetime
    //     foreach($arMsg as $msg)
    //     $stEntry = $msg;
    //     } else {   //concatenate msg with datetime
    //         $stEntry = $arMsg;
    //     }
    //
    //     //create file with current date name
    //     $stCurLogFileName='_token.txt';
    //     //open the file append mode,dats the log file will create day wise
    //     $fHandler=fopen('infusion'.$stCurLogFileName,'w');
    //     //write the info into the file
    //     fwrite($fHandler,$stEntry);
    //     //close handler
    //     fclose($fHandler);
    // }
    // m_log(json_encode($response, true));
// } else {
//     echo '<pre>';
//     print_r(json_decode($response,true));
//     echo '</pre>';
// }



// print_r($stCurLogFileName);
//
// first_name = given_name
// last_name = family_name
//
//
// "given_name": "Iron",
// "family_name": "herrero"
//
// "phone_numbers": [
//     {
//       "number": "5541997846305",
//       "extension": null,
//       "field": "PHONE1",
//       "type": "MOBILE"
//     }
// ],
