<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$code = $_GET['code'];

$options = array(
    'client_id' => '{CLIEND_ID}',
    'redirect_uri' => '{REDIRECT_URI}',
);
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.infusionsoft.com/token/",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "client_id={CLIEND_ID}&client_secret={CLIENT_SECRET}&code=".$code."&grant_type=authorization_code&redirect_uri=".$options['redirect_uri'],
  CURLOPT_HTTPHEADER => array(
    "content-type: application/x-www-form-urlencoded"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
// $response = json_decode($response,true);

// First Get the $_POST info and debug.
function m_log($arMsg){
    //define empty string
    $stEntry="";
    //get the event occur date time,when it will happened
    $arLogData['event_datetime']='['.date('D Y-m-d h:i:s A').'] [client '.$_SERVER['REMOTE_ADDR'].']';
    //if message is array type
    if(is_array($arMsg))
    {
    //concatenate msg with datetime
    foreach($arMsg as $msg)
    $stEntry = $msg;
    } else {   //concatenate msg with datetime
        $stEntry = $arMsg;
    }

    //create file with current date name
    $stCurLogFileName='_token.txt';
    //open the file append mode,dats the log file will create day wise
    $fHandler=fopen('infusion'.$stCurLogFileName,'w');
    //write the info into the file
    fwrite($fHandler,$stEntry);
    //close handler
    fclose($fHandler);
}
m_log(json_encode($response, true));
